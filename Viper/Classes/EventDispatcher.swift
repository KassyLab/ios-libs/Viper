//
// Created by Danglard Damien on 22/08/2018.
// Copyright (c) 2018 KassyLab. All rights reserved.
//

import Foundation

public protocol EventDispatcher {

    associatedtype EventHandlerType: EventHandler

    var eventHandler: EventHandlerType {get set}
}
