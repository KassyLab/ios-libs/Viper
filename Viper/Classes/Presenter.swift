//
// Created by Danglard Damien on 23/08/2018.
// Copyright (c) 2018 KassyLab. All rights reserved.
//

import Foundation

public protocol Presenter: EventHandler {

    associatedtype ViewType: View

    var view: ViewType {get}
}
